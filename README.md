# Vue template project

Template for a typescript vue project
## Project setup
```
npm install
```

### Run development server with hot-reload
```
npm run dev
```

### Build for production
```
npm run build
```

### Run jest tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Prettier check or fix files
```
npm run prettier:check
npm run prettier:write
```