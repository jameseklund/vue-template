import { shallowMount } from '@vue/test-utils'
import About from '../About.vue'

describe('About', () => {
  it('should match snapshot', () => {
    const wrapper = shallowMount(About)
    expect(wrapper.html()).toMatchInlineSnapshot('"<h1>About</h1>"')
  })
})
